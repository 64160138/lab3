import java.util.Scanner;

public class Problem4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int x, sum = 0, count = 0;
        while (true) {
            System.out.print("Please input number : ");
            x = sc.nextInt();
            if (x == 0)
                break;
            sum = sum + x;
            count++;
            System.out.println("Sum: " + sum + " Avg: " + ((double) sum / count));
        }
        System.out.println("Bye!");
        sc.close();
    }
}
