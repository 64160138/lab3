import java.util.Scanner;

public class Problem9While {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int i = 0;
        System.out.println("Please input n: ");
        int num = sc.nextInt();

        while (i != num) {
            for (int j = 1; j <= num; j++) {
                System.out.print(j);
            }
            System.out.println();
            i += 1;
        }
    }
}
