import java.util.Scanner;

public class Problem3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int x, y;
        x = sc.nextInt();
        y = sc.nextInt();
        if (x < y) {
            for (int i = x; i <= y; i++) {
                System.out.println(i + " ");
            }
        } else if (x == y) {
            System.out.println(x);
        } else {
            System.out.println("Error");
        }
    }
}
